# txt2pnm

Convert text to PNM.

## Build

Install the development package of the FreeType library,
e.g. for Debian:

```
apt-get install libfreetype-dev
```

Clone the repository and enter the directory:

```
git clone https://gitlab.com/v0g/txt2pnm.git
cd txt2pnm
```

Compile the tool:

```
make
```

## Usage

Call without arguments to see the usage description:

```
./txt2pnm
```

Usage description:

```
Usage:

    ./txt2pnm FONT_FILE FONT_HEIGHT TEXT_TO_RENDER > OUTPUT.pnm

Example:

    ./txt2pnm /usr/share/fonts/truetype/freefont/FreeSans.ttf 64 "It works!" > test.pnm
```

## Self-test

Run a simple self-test

```
make check
```

This renders the text "txt2pnm" and performs an ad-hoc conversion of
the PNM output image to ASCII art.

Expected output:

```
.#.
 #.       #. #####
.#...  ...#..#  .#....#.  ...#. ...#. .#.
###.#..#.###..  .#..##### .#####.########
 #. ###.  #.   .## .#. .#..#  .#.#  ##  #
 #.  ##   #.  ###  .#   #..#  .#.#  #.  #
 #. .##.  #. ##.   .#   #..#  .#.#  #.  #
 #. #.##  #..#.... .#. .#..#  .#.#  #.  #
 #### .#. ########..##### .#  .#.#  #.  #
 ..       ..       .# ..
                   .#
                   .#
```
