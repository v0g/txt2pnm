/*
 * Copyright (C) Volker Diels-Grabsch <v@njh.eu> https://njh.eu/
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#define FONT_HEIGHT 12
#define IMAGE_WIDTH  1024
#define IMAGE_HEIGHT 1024

unsigned char image[IMAGE_HEIGHT][IMAGE_WIDTH];

int main(int argc, char **argv)
{
    FT_Error error;
    FT_Library library;
    FT_Face face;
    FT_ULong charcode;
    char *fontfile;
    char *text;
    char *pos;
    FT_UInt font_height;
    FT_GlyphSlot slot;
    FT_Bitmap *bitmap;
    unsigned char bitmap_pixel;
    FT_Matrix matrix;
    FT_Vector pen;
    FT_UInt bitmap_x, bitmap_y;
    FT_Int image_x, image_y;
    FT_Int min_image_x, min_image_y;
    FT_Int max_image_x, max_image_y;
    if (argc != 4) {
        fprintf(stderr, "\n");
        fprintf(stderr, "Usage:\n");
        fprintf(stderr, "\n");
        fprintf(stderr, "    %s FONT_FILE FONT_HEIGHT TEXT_TO_RENDER > OUTPUT.pnm\n", argv[0]);
        fprintf(stderr, "\n");
        fprintf(stderr, "Example:\n");
        fprintf(stderr, "\n");
        fprintf(stderr, "    %s /usr/share/fonts/truetype/freefont/FreeSans.ttf 64 \"It works!\" > test.pnm\n", argv[0]);
        fprintf(stderr, "\n");
        return 1;
    }
    fontfile = argv[1];
    font_height = atoi(argv[2]);
    text = argv[3];
    error = FT_Init_FreeType(&library);
    if (error) {
        fprintf(stderr, "FT_Init_FreeType(...) failed with error 0x%x.\n", error);
        return 1;
    }
    error = FT_New_Face(library, fontfile, 0, &face);
    if (error) {
        fprintf(stderr, "FT_New_Face(library, ...) failed with error 0x%x.\n", error);
        return 1;
    }
    error = FT_Set_Pixel_Sizes(face, 0, font_height);
    if (error) {
        fprintf(stderr, "FT_Set_Pixel_Sizes(face, 0, %d) failed with error 0x%x.\n", font_height, error);
        return 1;
    }
    pen.x = 0;
    pen.y = (IMAGE_HEIGHT - font_height) * 64;
    matrix.xx = (FT_Fixed)(1 * 0x10000L);
    matrix.xy = (FT_Fixed)(0 * 0x10000L);
    matrix.yx = (FT_Fixed)(0 * 0x10000L);
    matrix.yy = (FT_Fixed)(1 * 0x10000L);
    for (image_y = 0; image_y < IMAGE_HEIGHT; image_y++) {
        for (image_x = 0; image_x < IMAGE_WIDTH; image_x++) {
            image[image_y][image_x] = 0;
        }
    }
    min_image_x = IMAGE_WIDTH;
    min_image_y = IMAGE_HEIGHT;
    max_image_x = 0;
    max_image_y = 0;
    for (pos = text; *pos; pos++) {
        FT_Set_Transform(face, &matrix, &pen);
        charcode = (FT_ULong)(*pos);
        error = FT_Load_Char(face, charcode, FT_LOAD_RENDER);
        slot = face->glyph;
        if (error) {
            fprintf(stderr, "FT_Load_Char(face, %ld, ...) failed with error 0x%x.\n", charcode, error);
            return 1;
        }
        if (slot->format != FT_GLYPH_FORMAT_BITMAP) {
            fprintf(stderr, "FT_Load_Char(face, %ld, ...) did not render in FT_GLYPH_FORMAT_BITMAP format.\n", charcode);
        }
        bitmap = &slot->bitmap;
        if (bitmap->pixel_mode != FT_PIXEL_MODE_GRAY) {
            fprintf(stderr, "FT_Load_Char(face, %ld, ...) did not render in FT_PIXEL_MODE_GRAY mode.\n", charcode);
        }
        for (bitmap_y = 0; bitmap_y < bitmap->rows; bitmap_y++) {
            for (bitmap_x = 0; bitmap_x < bitmap->width; bitmap_x++) {
                image_x = slot->bitmap_left + bitmap_x;
                image_y = IMAGE_HEIGHT - 1 - (slot->bitmap_top - bitmap_y);
                if (!(0 <= image_x && image_x < IMAGE_WIDTH && 0 <= image_y && image_y < IMAGE_HEIGHT)) {
                    fprintf(stderr, "Image buffer too small (or text too long).\n");
                    return 1;
                }
                bitmap_pixel = bitmap->buffer[bitmap_x + (bitmap_y * bitmap->width)];
                if (bitmap_pixel != 0 && image[image_y][image_x] < bitmap_pixel) {
                    image[image_y][image_x] = bitmap_pixel;
                    if (image_x < min_image_x) {
                        min_image_x = image_x;
                    }
                    if (image_x > max_image_x) {
                        max_image_x = image_x;
                    }
                    if (image_y < min_image_y) {
                        min_image_y = image_y;
                    }
                    if (image_y > max_image_y) {
                        max_image_y = image_y;
                    }
                }
            }
        }
        pen.x += slot->advance.x;
        pen.y += slot->advance.y;
    }
    if (max_image_x < min_image_x || max_image_y < min_image_y) {
        fprintf(stderr, "Empty image, most likely because of empty text.\n");
        return 1;
    }
    printf("P2\n");
    printf("%d %d\n", (max_image_x - min_image_x) + 1, (max_image_y - min_image_y) + 1);
    printf("255\n");
    for (image_y = min_image_y; image_y <= max_image_y; image_y++) {
        for (image_x = min_image_x; image_x <= max_image_x; image_x++) {
            if (image_x != 0) {
                printf(" ");
            }
            printf("%d", image[image_y][image_x]);
        }
        printf("\n");
    }
    FT_Done_Face(face);
    FT_Done_FreeType(library);
    return 0;
}
