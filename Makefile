default: txt2pnm

PKGS := freetype2

CFLAGS := $(shell pkg-config --cflags $(PKGS))
LIBS   := $(shell pkg-config --libs   $(PKGS))

txt2pnm: txt2pnm.c
	gcc -O3 -Werror -Wall -Wextra $(CFLAGS) $< $(LIBS) -o $@

check: txt2pnm
	./txt2pnm /usr/share/fonts/truetype/freefont/FreeSans.ttf 16 "txt2pnm" \
	  | tail -n +4 \
	  | sed 's/[1-9][0-9][0-9]/#/g; s/[1-9][0-9]/./g; s/ //g; s/[0-9]/ /g'

clean:
	rm -f txt2pnm
